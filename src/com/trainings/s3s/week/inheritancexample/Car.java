package com.trainings.s3s.week.inheritancexample;

public class Car extends Automotive{
	
	//IS-A relationship should be satisfied
	// car is-a automotive
	
	
	
	public void move() {
		System.out.println("car is moving");

	}
	
	
}
