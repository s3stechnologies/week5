package com.trainings.s3s.week.inheritancexample;

public class Automotive {
	
	 private String color;
	 private String tires;
	
		public String getColor() {
			return color;
		}

		public void setColor(String color) {
			this.color = color;
		}

		public String getTires() {
			return tires;
		}

		public void setTires(String tires) {
			this.tires = tires;
		}

		private void move() {
			System.out.println("It's moving");

		}
		
		public void getMove() {
			move();
		}
		

}
