package com.training.s3s.week.five;


// calling our own constructor
public class Student {
	
	private String name;
	private String id;
	private int rollNumber;
	
	
	public Student(String name, String id, int rollNumber) {
		this(id, rollNumber);
	
		System.out.println("Contructor with 3 paramerts");
		
		this.name = name;
		
	}
	
	public Student(String id, int rollNumber) {
		this(rollNumber);
		System.out.println("Contructor with 2 paramerts");

		this.id = id;
	}

	public Student(int rollNumber) {
		
		System.out.println("Contructor with 1 paramerts");
		
		this.rollNumber = rollNumber;
	}

	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getRollNumber() {
		return rollNumber;
	}
	public void setRollNumber(int rollNumber) {
		this.rollNumber = rollNumber;
	}
	
}
