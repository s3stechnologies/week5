package com.training.s3s.week.five;

public class ConstructorExample {
	
	 private String name;
	 private int id; 
	 private double salary;
	 private String socialSecurityNumber = "698797097";
	 
	
	public ConstructorExample(String name, int id, double salary, String socialSecurityNumber) {
		
		this.name = name;     // this means I , I mean is the object that I am..
		this.id = id;		
		this.salary = salary;
		this.socialSecurityNumber = socialSecurityNumber;
		
		
	}
	

	public String getName() {
		return name;
	}

	
	public int getId() {
		return id;
	}

	public double getSalary() {
		
		return salary;
	}


//	public void setSalary(double salary) {
//		if(salary >= 0) {
//		this.salary = salary;
//		}
//	}


	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	@Override
	public String toString() {
		return "ConstructorExample [name=" + name + ", id=" + id + ", salary=" + salary + ", socialSecurityNumber="
				+ socialSecurityNumber + "]";
	}

	
	
	
	
	
	
}
